import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import {DragDropModule} from '@angular/cdk/drag-drop';
import { AppComponent } from './app.component';
import { InputButtonUnitComponent } from './Components/input-button-unit/input-button-unit.component';
import { TodoItemComponent } from './Components/todo-item/todo-item.component';
import { ListManagerComponent } from './Components/list-manager/list-manager.component';
import { TodoListService } from './services/todo-list.service';
import { StorageService } from './services/storage.service';


@NgModule({
  declarations: [
    AppComponent,
    InputButtonUnitComponent,
    TodoItemComponent,
    ListManagerComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    DragDropModule
  ],
  providers: [TodoListService,
              StorageService
              ],
  bootstrap: [AppComponent]
})
export class AppModule { }
